<?php if (!defined('APPLICATION')) exit();

// File loginguard.defines.php must be included by manually specifying the whole
// path. It will then define some shortcuts for commonly used paths.
require(PATH_PLUGINS . '/LoginGuard/lib/loginguard.defines.php');
require(LOGINGUARD_PLUGIN_CLASSES_PATH . '/class.logincontroller.php');

// Define the plugin:
$PluginInfo['LoginGuard'] = array(
  'Name' => 'LoginGuard',
  'Description' => 'Improves the security of Login process by throttling and restricting failed attempts.',
  'Version' => '13.05.08',
  'RequiredApplications' => array('Vanilla' => '2.0.10'),
  'RequiredTheme' => FALSE,
  'RequiredPlugins' => array('Logger' => '12.10.28'),
  'HasLocale' => FALSE,
  'MobileFriendly' => TRUE,
  'SettingsUrl' => '/plugin/loginguard',
  'SettingsPermission' => 'Garden.Settings.Manage',
  'Author' => 'Diego Zanella',
  'AuthorEmail' => 'support@aelia.co',
  'AuthorUrl' => 'http://aelia.co',
	'RegisterPermissions' => array('Plugins.LoginGuard.Manage',),
);

class LoginGuardPlugin extends Gdn_Plugin {
	// @var Logger The Logger used by the class.
	private $_Log;

	/**
	 * Returns the instance of the Logger used by the class.
	 *
	 * @param Logger An instance of the Logger.
	 */
	protected function Log() {
		if(empty($this->_Log)) {
			$this->_Log = LoggerPlugin::GetLogger();
		}

		return $this->_Log;
	}

  /**
   * Base_Render_Before Event Hook
   *
   * @param Controller Sender Sending controller instance.
   */
  public function Base_Render_Before($Sender) {
    $Sender->AddCssFile($this->GetResource('design/loginguard.css', FALSE, FALSE));
    $Sender->AddJsFile($this->GetResource('js/loginguard.js', FALSE, FALSE));
  }

	/**
	 * Set Validation Rules related to Configuration Model.
	 *
	 * @param Gdn_Validation $Validation The Validation that is (or will be)
	 * associated to the Configuration Model.
	 *
	 * @return void
	 */
	protected function _SetConfigModelValidationRules(Gdn_Validation $Validation) {
		$Validation->ApplyRule('Plugin.LoginGuard.LoginAttemptsLimit', 'Required', T('Please specify a value for Login Attempts Limit.'));
		$Validation->ApplyRule('Plugin.LoginGuard.LoginAttemptsLimit', 'PositiveInteger', T('Login Attempts Limit must be a positive Integer.'));

		$Validation->ApplyRule('Plugin.LoginGuard.DelayAfterFailedAttempts', 'Required', T('Please specify the delay to apply after too many failed login attempts.'));
		$Validation->ApplyRule('Plugin.LoginGuard.DelayAfterFailedAttempts', 'PositiveInteger', T('Delay must be a positive Integer.'));

		$Validation->ApplyRule('Plugin.LoginGuard.IncreaseDelay', 'Required', T('Please specify a value for Increase Delay flag.'));

		$Validation->ApplyRule('Plugin.LoginGuard.LoginDelayIncreasePeriod', 'Required', T('Please specify a value for the period used to determine if ban duration must be increased progressively.'));
		$Validation->ApplyRule('Plugin.LoginGuard.LoginDelayIncreasePeriod', 'PositiveInteger', T('Ban increase period must be a positive Integer.'));
	}

  /**
   * Create a method called "LoginGuard" on the PluginController
   *
   * @param Sender Sending controller instance.
   */
  public function PluginController_LoginGuard_Create($Sender) {
    /*
     * If you build your views properly, this will be used as the <title> for your page, and for the header
     * in the dashboard. Something like this works well: <h1><?php echo T($this->Data['Title']); ?></h1>
     */
    $Sender->Title('LoginGuard Plugin');
    $Sender->AddSideMenu('plugin/loginguard');

    // If your sub-pages use forms, this is a good place to get it ready
    $Sender->Form = new Gdn_Form();

    $this->Dispatch($Sender, $Sender->RequestArgs);
  }

	/**
	 * Renders the Index page for the plugin.
	 *
   * @param Sender Sending controller instance.
   */
  public function Controller_Index($Sender) {
		Redirect(LOGINGUARD_THROTTLEDUSER_LIST_URL);
  }

	/**
	 * Alias for Controller_ThrottledUsers() method.
	 *
	 * @see Controller_ThrottledUsers().
	 */
	public function Controller_TUView($Sender) {
		return $this->Controller_ThrottledUsers($Sender);
	}

	/**
	 * Alias for Controller_ThrottlesUserDelete() method.
	 *
	 * @see Controller_ThrottlesUserDelete().
	 */
	public function Controller_TUDelete($Sender) {
		return $this->Controller_ThrottlesUserDelete($Sender);
	}

	/**
	 * Renders the page showing the list of currently throttled Users.
	 *
	 * @param object Sender Sending controller instance.
	 */
	public function Controller_ThrottledUsers($Sender) {
		$Sender->SetData('CurrentPath', LOGINGUARD_THROTTLEDUSER_LIST_URL);

		// Prevent Users without proper permissions from accessing this page.
		$Sender->Permission('Plugins.LoginGuard.Manage');

		$ThrottledUsersModel = new ThrottledUsersModel();
		$ThrottledUsersDataSet = $ThrottledUsersModel->GetCurrentlyThrottled();

		$Sender->SetData('ThrottledUsersDataSet', $ThrottledUsersDataSet);
		$Sender->Render($this->GetView('loginguard_throttledusers_view.php'));
	}

	/**
	 * Renders the page that asks for confirmation before deleting throttling
	 * information for a User.
	 *
	 * @param object Sender The Sender generated by the Request.
	 */
	public function Controller_ThrottlesUserDelete($Sender) {
		// Prevent Users without proper permissions from accessing this page.
		$Sender->Permission('Plugins.LoginGuard.Manage');

		$ThrottledUsersModel = new ThrottledUsersModel();

		$Sender->Form->SetModel($ThrottledUsersModel);

		// If seeing the form for the first time...
		if ($Sender->Form->AuthenticatedPostBack() === FALSE) {
			$UserID = $Sender->Request->GetValue(LOGINGUARD_ARG_USERID, null);

			// Load the data of the Appender to be edited, if an Appender ID is passed
			$ThrottlingInfo = $ThrottledUsersModel->GetForUser($UserID)->FirstRow(DATASET_TYPE_ARRAY);

			$Sender->Form->SetData($ThrottlingInfo);

			// Apply the config settings to the form.
			$Sender->Render($this->GetView('loginguard_delete_confirm_view.php'));
		}
		else {
			$Data = $Sender->Form->FormValues();

			// The field named "OK" is actually the OK button. If it exists, it means
			// that the User confirmed the deletion.
			if(Gdn::Session()->ValidateTransientKey(GetValue('TransientKey', $Data)) && isset($Data['OK'])) {
				// Delete Client Id
				$ThrottledUsersModel->Delete(GetValue('UserID', $Data));

				$Sender->InformMessage(T('Throttling deleted.'));
			}
			Redirect(LOGINGUARD_THROTTLEDUSER_LIST_URL);
		}
	}

	/**
	 * Displays the Settings page.
	 *
   * @param $Sender Sending controller instance.
	 */
	public function Controller_Settings($Sender) {
		$Sender->SetData('CurrentPath', LOGINGUARD_GENERALSETTINGS_URL);
		// Prevent non-admins from accessing this page
		$Sender->Permission('Plugins.LoginGuard.Manage');

		$Validation = new Gdn_Validation();
		$this->_SetConfigModelValidationRules($Validation);

		// TODO Allow User to choose if he wants to send an email to the User when a login fails.
		$ConfigurationModel = new Gdn_ConfigurationModel($Validation);
		$ConfigurationModel->SetField(array(
			'Plugin.LoginGuard.LoginAttemptsLimit' => LOGINGUARD_DEFAULT_ATTEMPTSLIMIT,
			'Plugin.LoginGuard.DelayAfterFailedAttempts' => LOGINGUARD_DEFAULT_DELAYAFTERFAILEDATTEMPTS,
			'Plugin.LoginGuard.IncreaseDelay' => LOGINGUARD_DEFAULT_INCREASEDELAY,
			'Plugin.LoginGuard.LoginDelayIncreasePeriod' => LOGINGUARD_DEFAULT_DELAYINCREASEPERIOD,
			'Plugin.LoginGuard.NotifyThrottledUser' => LOGINGUARD_DEFAULT_NOTIFYTHROTTLEDUSER,
		));

		// Set the model on the form.
		$Sender->Form->SetModel($ConfigurationModel);

		// If seeing the form for the first time...
		if ($Sender->Form->AuthenticatedPostBack() === FALSE) {
			// Apply the config settings to the form.
			$Sender->Form->SetData($ConfigurationModel->Data);
		}
		else {
			$Saved = $Sender->Form->Save();
			if ($Saved) {
				$Sender->InformMessage(T('Your changes have been saved.'));
			}
		}

		$Sender->Render($this->GetView('loginguard_generalsettings_view.php'));
	}

	/**
	 * Incapsulates several preparation steps that are normally performed by the
	 * EntryController before validating the login.
	 * They have been grouped together in this method because they don't
	 * really take part either in Login Validation, or the User login process
	 * itself, but they merely add some display information.
	 *
	 * @param Controller Sender Sending Controller instance.
	 */
	private function PrepareSender($Sender) {
		$Sender->View = $this->GetView('loginguard_signin.php');
		$Sender->AddJsFile('entry.js');
		$Sender->SetData('Title', T('Sign In'));
		$Sender->Form->AddHidden('Target', $Sender->Target());
		$Sender->Form->AddHidden('ClientHour', date('Y-m-d H:00')); // Use the server's current hour as a default.

		// Additional signin methods are set up with plugins.
		$Methods = array();
		$Sender->SetData('MainFormArgs', array(null));
		$Sender->SetData('Methods', $Methods);
		$Sender->SetData('FormUrl', Url('entry/signin'));
	}

	/**
	 * Designed to replace the standard Login method on Entry Controller, it
	 * validates User credentials and permissions, and logs the User in when
	 * everything is fine. In addition to the standard login process, it also
	 * implements a throttling mechanism, aimed at slowing down attacker who try
	 * to log in using brute force methods.
	 *
	 * @param Controller Sender Sending Controller instance.
	 */
	public function EntryController_LgSignIn_Create($Sender) {
		$this->PrepareSender($Sender);
		$Sender->FireEvent('SignIn');

		// If it's a PostBack, try to sign in the User
		if($Sender->Form->IsPostBack()) {
			$Sender->Form->ValidateRule('Email', 'ValidateRequired', sprintf(T('%s is required.'), T('Email/Username')));
			$Sender->Form->ValidateRule('Password', 'ValidateRequired');

			// If User Identifier and Password have been passed, check if User can log in
			if ($Sender->Form->ErrorCount() == 0) {
				$UserIdentifier = $Sender->Form->GetFormValue('Email');
				$Password = $Sender->Form->GetFormValue('Password');
				$RememberUser = $Sender->Form->GetFormValue('RememberMe');
				$ClientHour = $Sender->Form->GetFormValue('ClientHour');

				$this->Log()->debug(sprintf(T('Starting login validation. User Identifier: "%s"'),
																		$UserIdentifier));

				$LoginErrors = array();

				$LoginController = new LoginController();
				$LoginResult = $LoginController->ValidateLogin($UserIdentifier,
																											 $Password,
																											 $RememberUser,
																											 $ClientHour,
																											 $User,
																											 $LoginErrors);

				// Add to the Sender's form all the error messages returned by login validation
				$this->SetFormErrors($Sender->Form, $LoginErrors);

				switch($LoginResult) {
					// Invalid Password - Throttle after several failed attempts
					case LOGINGUARD_ERR_INVALID_PASSWORD:
						// TODO Keep count of the remaining login attempts available before being throttled (such as "you failed X of Y attempts").
						$this->Log()->info(sprintf(T('Invalid password entered for User "%s". IP Address: %s.'),
																		 $Sender->Form->GetFormValue('Email'),
																		 Gdn::Request()->IpAddress()
																		 ));
						break;
					// Valid credentials - OK
					case LOGINGUARD_OK:
						$this->SetRedirect($Sender);
						break;
				}
			}
		}
		else {
			if($Target = $Sender->Request->Get('Target')) {
				$Sender->Form->AddHidden('Target', $Target);
			}
			$Sender->Form->SetValue('RememberMe', TRUE);
		}

		return $Sender->Render();
	}

	/**
	 * Takes an array of error messages and adds them to a Form.
	 *
	 * @param Gdn_Form Form The form to which the errors will be added.
	 * @param array Errors The array of error messages.
	 */
	private function SetFormErrors(Gdn_Form $Form, array $Errors) {
		foreach($Errors as $Error) {
			$Form->AddError($Error);
		}
	}

	/**
	 * Redirects User to destination upon successful login.
	 *
	 * @param EntryController Sender The instance of the EntryController which
	 * handles the request.
	 */
	protected function SetRedirect($Sender) {
		$Url = Url($Sender->RedirectTo(), TRUE);

		$Sender->RedirectUrl = $Url;
		$Sender->MasterView = 'empty';
		$Sender->View = 'redirect';

		if($Sender->DeliveryType() != DELIVERY_TYPE_ALL) {
			$Sender->DeliveryMethod(DELIVERY_METHOD_JSON);
			$Sender->SetHeader('Content-Type', 'application/json');
		}
	}

  /**
   * Add a link to the dashboard menu
   *
   * By grabbing a reference to the current SideMenu object we gain access to its methods, allowing us
   * to add a menu link to the newly created /plugin/LoginGuard method.
   *
   * @param $Sender Sending controller instance.
   */
  public function Base_GetAppSettingsMenuItems_Handler($Sender) {
    $Menu = $Sender->EventArguments['SideMenu'];
    $Menu->AddLink('Add-ons', 'LoginGuard', 'plugin/loginguard', 'Garden.Settings.Manage');
  }

  /**
   * Plugin setup
   *
   * This method is fired once, immediately after the plugin has been enabled.
   */
  public function Setup() {
    // Set up the plugin's default values
		SaveToConfig('Plugin.LoginGuard.LoginAttemptsLimit', LOGINGUARD_DEFAULT_ATTEMPTSLIMIT);
		SaveToConfig('Plugin.LoginGuard.DelayAfterFailedAttempts', LOGINGUARD_DEFAULT_DELAYAFTERFAILEDATTEMPTS);
		SaveToConfig('Plugin.LoginGuard.IncreaseDelay', LOGINGUARD_DEFAULT_INCREASEDELAY);
		SaveToConfig('Plugin.LoginGuard.LoginDelayIncreasePeriod', LOGINGUARD_DEFAULT_DELAYINCREASEPERIOD);
		SaveToConfig('Plugin.LoginGuard.NotifyThrottledUser', LOGINGUARD_DEFAULT_NOTIFYTHROTTLEDUSER);

		// Create Route to redirect calls to /discussions to /listdiscussions
		Gdn::Router()->SetRoute('^entry/signin(/.*)?$',
														LOGINGUARD_SIGNIN_URL . '/$1',
														'Internal');

		// Create Database Objects needed by the Plugin
		require('install/loginguard.schema.php');
		LoginGuardSchema::Install();
  }

  /**
   * Plugin cleanup
   *
   * This method is fired once, immediately before the plugin is disabled.
   */
  public function OnDisable() {
		Gdn::Router()->DeleteRoute('^entry/signin(/.*)?$');
  }

	/**
	 * Plugin cleanup
	 *
	 * This method is fired once, when the plugin is uninstalled.
	 */
	public function CleanUp() {
		// Remove Plugin's configuration parameters
		RemoveFromConfig('Plugin.LoginGuard.LoginAttemptsLimit');
		RemoveFromConfig('Plugin.LoginGuard.DelayAfterFailedAttempts');
		RemoveFromConfig('Plugin.LoginGuard.IncreaseDelay');
		RemoveFromConfig('Plugin.LoginGuard.LoginDelayIncreasePeriod');
		RemoveFromConfig('Plugin.LoginGuard.NotifyThrottledUser');

		require('install/loginguard.schema.php');
		LoginGuardSchema::Uninstall();
	}
}
