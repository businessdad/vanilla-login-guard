<?php if (!defined('APPLICATION')) exit();



$Methods = $this->Data('Methods', array());
$SelectedMethod = $this->Data('SelectedMethod', array());
$CssClass = count($Methods) > 0 ? ' MultipleEntryMethods' : ' SingleEntryMethod';

?>
<div class="Entry <?php echo $CssClass ?>">
	<h1><?php echo $this->Data('Title') ?></h1>

	<div class="MainForm">
		<?php
			echo GetValue('ThrottlingWarning', $this->Data, '');

			include $this->FetchViewLocation('passwordform');
			echo $this->Data('MainForm');
		?>
	</div>
	<div class="Methods">
		<?php
			if (count($Methods) > 0) {
				echo Wrap('<strong>'.T('Or you can...').'</strong>',
									'div');
			}
      foreach ($Methods as $Key => $Method) {
         $CssClass = 'Method Method_'.$Key;
         echo Wrap($Method['SignInHtml'],
									 'div',
									 array('class' => $CssClass,)
									);
      }
		?>
	</div>
</div>
