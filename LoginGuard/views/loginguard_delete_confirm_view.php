<?php if (!defined('APPLICATION')) exit();


?>
<div class="Logger ConfirmationDialog">
	<?php
		echo $this->Form->Open();
		echo $this->Form->Errors();
	?>
	<fieldset>
		<div class="Title">
			<h3><?php echo T('Confirmation'); ?></h3>
		</div>
		<div class="Message">
			<p>
				<?php
					echo Wrap(sprintf(T('You\'re about to remove throttling for User <strong>%s</strong> (User ID: <strong>%d</strong>). This will allow ' .
															'the User to attempt to log in again.'),
														$this->Form->GetValue('UserName'),
														$this->Form->GetValue('UserID')),
										'div',
										array('class' => 'Info',)
										);
					echo Wrap(T('Would you like to continue? This operation cannot be undone!'),
										'div',
										array('class' => 'Info',)
										);
				?>
			</p>
		</div>
		<div>
			<?php
				echo $this->Form->Hidden('UserID');
				echo $this->Form->Button(T('OK'), array('Name' => 'OK',));
				echo $this->Form->Button(T('Cancel'), array('Name' => 'Cancel',));
			?>
		</div>
	</fieldset>
	<?php
		 echo $this->Form->Close();
	?>
</div>
