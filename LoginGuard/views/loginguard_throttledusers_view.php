<?php if (!defined('APPLICATION')) exit();



// Indicates how many columns there are in the table that shows the list of
// throttled Users. It's mainly used to set the "colspan" attributes of
// single-valued table rows, such as Title, or the "No Results Found" message.
define('THROTTLEDUSERS_TABLE_COLUMNS', 6);

// The following HTML will be displayed when the DataSet is empty.
$OutputForEmptyDataSet = Wrap(T('No Users are being throttled.'),
															'td',
															array('colspan' => THROTTLEDUSERS_TABLE_COLUMNS,
																		'class' => 'NoResultsFound',)
															);

$ThrottledUsersDataSet = $this->Data['ThrottledUsersDataSet'];
?>
<div class="LoginGuardPlugin">
	<div class="Header">
		<?php include('loginguard_admin_header.php'); ?>
	</div>
	<div class="Content">
		<?php
			echo $this->Form->Open();
			echo $this->Form->Errors();
		?>
		<h3><?php echo T('Throttled Users'); ?></h3>
		<div class="Info">
			<?php
				echo Wrap(T('Here you can see a list of Users who are currently throttled.'), 'p');
			?>
		</div>
		<table id="ThrottledUsersList" class="display AltRows">
			<thead>
				<tr>
					<th><?php echo T('Name'); ?></th>
					<th><?php echo T('Last Login Attempt'); ?></th>
					<!--<th><?php echo T('Failed Logins'); ?></th>-->
					<th><?php echo T('Throttling Start'); ?></th>
					<th><?php echo T('Throttling End'); ?></th>
					<th><?php echo T('Throttling Duration'); ?></th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tfoot>
			</tfoot>
			<tbody>
				<?php
					$ThrottledUsersDataSet = $this->Data['ThrottledUsersDataSet'];

					// If DataSet is empty, just print a message.
					if(empty($ThrottledUsersDataSet) ||
						 ($ThrottledUsersDataSet->NumRows() <= 0)) {
						echo $OutputForEmptyDataSet;
					}

					// TODO Implement Pager.
					// Output the details of each row in the DataSet
					foreach($ThrottledUsersDataSet as $ThrottlingInfo) {
						// Output User Name
						echo Wrap(Gdn_Format::Text($ThrottlingInfo->UserName), 'td', array('class' => 'UserName',));
						// Output Date/Time of last login attempt
						echo Wrap(Gdn_Format::Text($ThrottlingInfo->LastLoginAttempt), 'td', array('class' => 'LastLoginAttempt',));
						// Output Number of failed logins
						//echo Wrap(Gdn_Format::Text($ThrottlingInfo->FailedLogins), 'td', array('class' => 'FailedLogins',));
						// Date/Time of Throttling Start
						echo Wrap(Gdn_Format::Text($ThrottlingInfo->ThrottlingStart), 'td', array('class' => 'ThrottlingStart',));
						// Date/Time of Throttling End
						echo Wrap(Gdn_Format::Text($ThrottlingInfo->ThrottlingEnd), 'td', array('class' => 'ThrottlingEnd',));
						// Duration of Throttling
						echo Wrap(Gdn_Format::Text($ThrottlingInfo->ThrottlingDuration) .
											T('&nbsp;minutes'), 'td', array('class' => 'ThrottlingDuration',));

						$DeleteLink = Anchor(T('Remove Throttling'),
																	LOGINGUARD_THROTTLEDUSER_DELETE_URL . '?' .
																	LOGINGUARD_ARG_USERID . '=' . $ThrottlingInfo->UserID,
																	'SmallButton DeleteThrottling',
																	array('title' => T('Delete the throttling for the User, enabling him/her ' .
																										 'to log in.'),)
																	);
						echo Wrap($DeleteLink,
											'td',
											array('class' => 'Delete',)
											);

						echo "</tr>\n";
					}
				?>
			 </tbody>
		</table>
		<?php
			 echo $this->Form->Close();
		?>
	</div>
</div>
