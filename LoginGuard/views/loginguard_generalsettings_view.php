<?php if (!defined('APPLICATION')) exit();

?>
<div class="LoginGuardPlugin">
	<div class="Header">
		<?php include('loginguard_admin_header.php'); ?>
	</div>
	<div class="Content">
		<?php
			echo $this->Form->Open();
			echo $this->Form->Errors();
		?>
		<fieldset>
			<legend>
				<h3><?php echo T('General Settings'); ?></h3>
				<p>
					<?php
					echo Wrap(T('In this section you can find some settings that will apply to the default (System) LoginGuard.'),
										'div',
										array('class' => 'Info',));
					?>
				</p>
			</legend>
			<div class="BanDurationExample Help Aside"><?php
				echo Wrap(T('How does Ban duration increase over time?'),
									'h4',
									array('class' => 'Title',));
				echo Wrap(T('The following example assumes a ban duration of <i>15 minutes</i> ' .
										'a Ban Increase Period of <i>24 hours</i>.'),
									'div',
									array('class' => 'Info',));
				echo "<ul>\n";
				echo Wrap(T('User X gets throttled for 15 minutes.'),
									'li');
				echo Wrap(T('After the ban period minutes, User X fails again multiple ' .
										'times and gets throttled once more.'),
									'li');
				echo Wrap(T('If this second throttle occurs within th past 24 hours, ' .
										'this second throttling will last 30 minutes (15 for the ' .
										'previous throttling + 15 for current one).'),
									'li');
				echo Wrap(T('Similarly, a third throttling in the same period would ' .
										'last 45 minutes, a fourth would last 60 minutes, and so on.'),
									'li');
				echo "</ul>\n";
			?></div>
			<ul>
				<li><?php
					echo $this->Form->Label(T('Throttle User after this amount of failed Login attempts'), 'Plugin.LoginGuard.LoginAttemptsLimit');
					echo Wrap(T('Specify after how many failed login attempts a User should be throttled, ' .
											'i.e. temporarily banned. During the ban period, any login attempt for the ' .
											'user will be rejected.'),
										'div',
										array('class' => 'Info',));
					echo $this->Form->TextBox('Plugin.LoginGuard.LoginAttemptsLimit',
																		 array('size' => '5',
																					 'maxlength' => '2'));
				?></li>
				<li><?php
					echo $this->Form->Label(T('Ban Duration'), 'Plugin.LoginGuard.DelayAfterFailedAttempts');
					echo Wrap(T('Specify for how long, <strong>in minutes</strong>, a User should be banned ' .
											'from attempting to log in after exceeded the maximum number of failed ' .
											'attempts specified above. '),
										'div',
										array('class' => 'Info',));
					echo $this->Form->TextBox('Plugin.LoginGuard.DelayAfterFailedAttempts',
																		 array('size' => '5',
																					 'maxlength' => '4'));
				?></li>
				<li><?php
					echo $this->Form->CheckBox('Plugin.LoginGuard.IncreaseDelay',
																		 T('Increase Ban Duration progressively.'),
																		 array('value' => '1',));
					echo Wrap(T('If checked, the ban duration will increase every time a User gets throttled ' .
											'in a specific period, unless he logs in successfully after a ban expires, or ' .
											'an Admin removes the throttling.'),
										'div',
										array('class' => 'Info',));
				?></li>
				<li><?php
					echo $this->Form->Label(T('Ban Increase period'), 'Plugin.LoginGuard.LoginDelayIncreasePeriod');
					echo Wrap(T('Specify, in <strong>hours</strong> the length of the period in which ban duration ' .
											'would increase if User gets throttled multiple times.'),
										'div',
										array('class' => 'Info',));
					echo $this->Form->TextBox('Plugin.LoginGuard.LoginDelayIncreasePeriod',
																		 array('size' => '5',
																					 'maxlength' => '4'));
				?></li>
			</ul>
		</fieldset>
		<fieldset>
			<legend>
				<h3><?php echo T('Notifications'); ?></h3>
				<p>
					<?php
					echo Wrap(T('In this section you can find some settings that will apply to the default (System) LoginGuard.'),
										'div',
										array('class' => 'Info',));
					?>
				</p>
			</legend>
			<ul>
				<li><?php
					echo $this->Form->CheckBox('Plugin.LoginGuard.NotifyThrottledUser',
																		 T('Notify Users when their accounts are throttled.'),
																		 array('value' => '1',));
					echo Wrap(T('If checked, an email will be sent to the Users whose accounts are throttled, ' .
											'i.e. temporarily banned.'),
										'div',
										array('class' => 'Info',));
				?></li>
			</ul>
		</fieldset>
		<?php
			 echo $this->Form->Close('Save');
		?>
	</div>
</div>
