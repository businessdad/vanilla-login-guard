<?php if (!defined('APPLICATION')) exit();


/**
 * Constants used by LoginGuard Plugin.
 */

// Default Configuration Settings

// Default number of failed login attempts before throttling
define('LOGINGUARD_DEFAULT_ATTEMPTSLIMIT', 5);
// Default amount of time for which login cannot be done with a throttled user
define('LOGINGUARD_DEFAULT_DELAYAFTERFAILEDATTEMPTS', 15);
// Indicates if the login delay should be increased after multiple failed
// attempts.
define('LOGINGUARD_DEFAULT_INCREASEDELAY', 1);
// Default period, expressed in hours, in which every subsequent throttling will
// increase the ban length.
define('LOGINGUARD_DEFAULT_DELAYINCREASEPERIOD', 24);
// Indicates if Users should be notified when their account is being throttled
define('LOGINGUARD_DEFAULT_NOTIFYTHROTTLEDUSER', 0);

// Paths
define('LOGINGUARD_PLUGIN_PATH', PATH_PLUGINS . '/LoginGuard');
define('LOGINGUARD_PLUGIN_LIB_PATH', LOGINGUARD_PLUGIN_PATH . '/lib');
define('LOGINGUARD_PLUGIN_CLASSES_PATH', LOGINGUARD_PLUGIN_LIB_PATH . '/classes');
define('LOGINGUARD_PLUGIN_VIEWS_PATH', LOGINGUARD_PLUGIN_PATH . '/views');

// URLs
define('LOGINGUARD_PLUGIN_BASE_URL', '/plugin/loginguard');
define('LOGINGUARD_SIGNIN_URL', '/entry/lgsignin');
define('LOGINGUARD_THROTTLEDUSER_LIST_URL', LOGINGUARD_PLUGIN_BASE_URL . '/tuview');
define('LOGINGUARD_THROTTLEDUSER_DELETE_URL', LOGINGUARD_PLUGIN_BASE_URL . '/tudelete');
define('LOGINGUARD_GENERALSETTINGS_URL', LOGINGUARD_PLUGIN_BASE_URL . '/settings');

// Return Codes
define('LOGINGUARD_OK', 0);
define('LOGINGUARD_ERR_INVALID_USERNAME', 1001);
define('LOGINGUARD_ERR_INVALID_PASSWORD', 1002);
define('LOGINGUARD_ERR_USER_THROTTLED', 1003);
define('LOGINGUARD_ERR_USER_NOT_ALLOWED_TO_LOG_IN', 1004);
//define('LOGINGUARD_ERR_INVALID_REQ_TYPE', 1005);
//define('LOGINGUARD_ERR_USER_NOT_FOUND', 1006);

// Http Arguments
define('LOGINGUARD_ARG_USERID', 'uid');
