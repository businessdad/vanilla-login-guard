<?php if (!defined('APPLICATION')) exit();


/**
 * Allows to read/write LGThrottledUsers table, which contains a list of the
 * Users who have been throttled due to too many failed login attempts.
 */
class ThrottledUsersModel extends Gdn_Model {
	/**
	 * Set Validation Rules that apply when saving a new row in Cron Jobs History.
	 *
	 * @return void
	 */
	protected function _SetThrottlesUsersValidationRules() {
		// Set additional Validation Rules here. Please note that formal validation
		// is done automatically by base Model Class, by retrieving Schema
		// Information.
	}

	/**
	 * Defines the related database table name.
	 */
	public function __construct() {
		parent::__construct('LGThrottledUsers');

		$this->_SetThrottlesUsersValidationRules();
	}

	/**
	 * Build SQL query to retrieve the list of throttled Users.
	 */
	protected function PrepareThrottledUsersQuery() {
		$Query = $this->SQL
			->Select('VTU.UserID')
			->Select('VTU.UserName')
			->Select('VTU.LastLoginAttempt')
			->Select('VTU.FailedLogins')
			->Select('VTU.ThrottlingStart')
			->Select('VTU.ThrottlingDuration')
			->Select('VTU.ThrottlingEnd')
			->Select('VTU.DateInserted')
			->Select('VTU.DateUpdated')
			->From('v_LGThrottledUsers VTU');

		return $Query;
	}

	/**
	 * Returns a DataSet containing a list of throttled Users.
	 *
	 * @param Wheres An associative array of [Field => Where clause] to add to the
	 * query.
	 * @param Limit Limit the amount of rows to be returned. Note: it doesn't
	 * apply to Summary Datasets, as they normally contain one row per total.
	 * @param Offset Specifies from which rows the data should be returned. Used
	 * for pagination. Note: it doesn't apply to Summary Datasets.
	 * @return Gdn_Dataset A DataSet containing a list of the throttled Users.
	 */
	public function Get(array $Wheres = null, $Limit = 1000, $Offset = 0) {
		// Set default Limit and Offset, if invalid ones have been passed.
		$Limit = (is_numeric($Limit) && $Limit > 0) ? $Limit : 1000;
		$Offset = (is_numeric($Offset) && $Offset > 0) ? $Offset : 0;

		// Return the Jobs Started within the Date Range.
		$this->PrepareThrottledUsersQuery();

		// Add WHERE clauses, if provided
		if(!empty($Wheres)) {
			$this->SQL
				->Where($Wheres);
		}

		$Result = $this->SQL
			->OrderBy('ThrottlingStart', 'desc')
			->Limit($Limit, $Offset)
			->Get();

		return $Result;
	}

	/**
	 * Returns a DataSet containing de details of the throttling for a single User.
	 *
	 * @param int UserID The ID of the User for whom to load the throttling information.
	 * @return Gdn_Dataset A DataSet containing the throttling information for
	 * the User.
	 */
	public function GetForUser($UserID) {
		return $this->Get(array('UserID' => $UserID));
	}

	public function GetCurrentlyThrottled() {
		$CurrentTime = date('Y-m-d H:i:s', gmmktime());
		return $this->Get(array('ThrottlingEnd >=' => $CurrentTime));
	}

	/*
	 * Adds a User to the Throttled Users table.
	 *
   * @param array $FormPostValues An associative array of $Field => $Value pairs
   * that represent data posted from the form.
   * @return The value of the Primary Key of the row that has been saved, or
   * FALSE if the operation could not be completed successfully.
	 */
	// TODO Fix Save() method as done for Awards Plugin (logic is not entirely correct and it can break due to DateInserted, DateUpdated etc not being populated)
	public function Save($FormPostValues) {
		// Define the primary key in this model's table.
		$this->DefineSchema();

		// Validate posted data
		if(!$this->Validate($FormPostValues)) {
			return false;
		}

		// Get the User ID
		$UserID = GetValue($this->PrimaryKey, $FormPostValues, false);
		$Insert = ($this->GetForUser($UserID)->NumRows() <= 0);
		//var_dump($FormPostValues, $Insert);

		// Prepare all the validated fields to be passed to an INSERT/UPDATE query
		$Fields = &$this->Validation->ValidationFields();
		if($Insert) {
			$this->AddInsertFields($Fields);
			return $this->Insert($Fields);
		}
		else {
			$this->AddUpdateFields($Fields);
			$this->Update($Fields, array($this->PrimaryKey => $UserID));
			return $UserID;
		}
	}

	/**
	 * Removes a row from LGThrottledUsers table.
	 *
	 * @param int UserID The User ID that identifies the row to be deleted.
	 */
	public function Delete($UserID) {
		$this->SQL->Delete('LGThrottledUsers', array('UserID' => $UserID,));
	}
}
