<?php


class LoginGuardPluginTestSuite extends PHPUnit_Vanilla_TestSuite {
	const PLUGIN_TESTS_DIR = 'plugin';
	const MODELS_TESTS_DIR = 'models';
	const CONTROLLERS_TESTS_DIR = 'controllers';

	protected static $_TestFiles = array(
		// Tests for Plugin's main class
		self::PLUGIN_TESTS_DIR => array(
		),
		// Tests for Plugin's Models
		self::MODELS_TESTS_DIR => array(
		),
		// Tests for Plugin's Controllers
		self::CONTROLLERS_TESTS_DIR => array(
			'class.logincontroller.test.php'
		),
	);

	/**
	 * Instantiates a Test Suite.
	 *
	 * @param array TestFiles A list of Test Files to load.
	 * @return PHPUnit_Vanilla_TestSuite A Test Suite instance.
	 */
	public static function suite() {
		$TestSuite = new LoginGuardPluginTestSuite();
		$TestSuite->addTestFiles(self::_GetTestFiles(__DIR__));
		return $TestSuite;
	}

	/**
	 * Test Suite setup.
	 */
	protected function setUp() {
		// Use function EnablePlugin() to enable all the plugins required, including
		// the one to be tested.
		$this->EnablePlugin('Logger');
		$this->EnablePlugin('LoginGuard');
  }

	/**
	 * Test Suite teardown.
	 */
  protected function tearDown() {
    print "\nMySuite::tearDown()";
	}
}
