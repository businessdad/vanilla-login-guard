<?php


// TODO Check why Tests are not running (verify that PHPUnit is initialised correctly)
class LoginControllerTests extends PHPUnit_Vanilla_TestCase {
	// @var LoginController The instance of the LoginController to be tested
	protected $LoginController;
	// @var int The ID of the User created for testing purposes
	protected $TestUserID;

	/**
	 * Test Suite initialization.
	 */
	protected function setUp() {
		// Create a User account to test the login
		$this->TestUserID = $this->CreateUser();

		// Instantiate the plugin to be tested.
		$this->LoginController = new LoginController();
	}

	/**
	 * Test Suite finalization and cleanup.
	 */
	protected function tearDown() {
		$this->LoginController = null;

		// Delete the User account created during Setup
		$this->DeleteUser($this->TestUserID);
	}

	/**
	 * Sample test, used to make sure that tests run.
	 */
	public function testUserIDSet() {
		$this->assertNotNull($this->TestUserID);
	}

	/**
	 * Attempt to register to the Cron Jobs list a null object.
	 *
	 * @expectedException InvalidArgumentException
	 * @expectedExceptionCode CRON_ERR_NOT_AN_OBJECT
	 */
	//public function testRegisterCronJob_NullObject() {
	//	$InvalidObject = null;
	//	$this->CronJobsPlugin->RegisterCronJob($InvalidObject);
	//}
	//
	///**
	// * Attempt to register to the Cron Jobs list an object which doesn't
	// * implement the required Cron() method.
	// *
	// * @expectedException InvalidArgumentException
	// * @expectedExceptionCode CRON_ERR_CRON_METHOD_UNDEFINED
	// */
	//public function testRegisterCronJob_CronMethodUndefined() {
	//	$InvalidObject = new stdClass();
	//	$this->CronJobsPlugin->RegisterCronJob($InvalidObject);
	//}
	//
	///**
	// * Attempt to register to the Cron Jobs list an object which implements
	// * the required Cron() as expected. For the purpose of this test, the
	// * internal CronJobsPlugin is used, as it already implements the required
	// * method. It wouldn't make much sense in real life to register the same
	// * object multiple times, however RegisterCronJob method uses object's
	// * class as a key, preventing duplicate registration.
	// *
	// */
	//public function testRegisterCronJob_ExpectedObject() {
	//	$this->assertTrue($this->CronJobsPlugin->RegisterCronJob($this->CronJobsPlugin),
	//										T('Failed to register plugin for Cron.'));
	//}
}
