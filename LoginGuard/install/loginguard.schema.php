<?php if (!defined('APPLICATION')) exit();


require('plugin.schema.php');

class LoginGuardSchema extends PluginSchema {
	/**
	 * Create the table which will store the list of the Users who are throttled,
	 * i.e. who can't perform the log in until a later date/time.
	 */
	protected function create_throttled_users_table() {
		Gdn::Structure()
			->Table('LGThrottledUsers')
			->Column('UserID', 'int', FALSE, 'primary')
			->Column('LastLoginAttempt', 'datetime', FALSE)
			->Column('FailedLogins', 'int', FALSE)
			->Column('ThrottlingStart', 'datetime', TRUE)
			->Column('ThrottlingDuration', 'int', TRUE)
			->Column('DateInserted', 'datetime', FALSE)
			->Column('DateUpdated', 'datetime', TRUE)
			->Set(FALSE, FALSE);
	}

	/**
	 * Creates a View that returns the list of the throttled Users.
	 */
	protected function create_throttled_users_view() {
		$Px = $this->Px;
		$Sql = "SELECT\n" .
					"	TU.UserID\n" .
					"	,TU.LastLoginAttempt\n" .
					"	,TU.FailedLogins\n" .
					"	,TU.ThrottlingStart\n" .
					"	,TU.ThrottlingDuration\n" .
					"	,DATE_ADD(TU.ThrottlingStart, INTERVAL TU.ThrottlingDuration MINUTE) AS ThrottlingEnd\n" .
					"	,TU.DateInserted\n" .
					"	,TU.DateUpdated\n" .
					"	,U.`Name` AS UserName\n" .
					"FROM\n" .
					"    {$Px}LGThrottledUsers TU" .
					"    JOIN" .
					"    {$Px}User U ON" .
					"    	(U.UserID = TU.UserID)";
		$this->Construct->View('v_LGThrottledUsers', $Sql);
	}

	/**
	 * Create all the Database Objects in the appropriate order.
	 */
	protected function CreateObjects() {
		$this->create_throttled_users_table();
		$this->create_throttled_users_view();
	}

	/**
	 * Delete the Database Objects.
	 */
	protected function DropObjects() {
		$this->DropView('v_LGThrottledUsers');
		$this->DropTable('LGThrottledUsers');
	}
}
